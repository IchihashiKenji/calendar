<?php
require_once __DIR__ . '/class/Calendar.class.php';
require_once __DIR__ . '/function/Encode.php';

$year = isset($_GET['y']) ? $_GET['y'] : date('Y');

//不正なパラメータを受け取った時は現在の年を表示
$year = (ctype_digit($year)) ? $year : date(Y);

$cal = new Calendar($year, 1);//0:日曜  1:月曜
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8"/>
    <title>PHPでカレンダー</title>
    <link rel="stylesheet" href="css/style.css"/>
    <script type="text/javascript" src="js/pop.js"></script>
</head>
<body>
    <table id="frame">
        <thead>
            <tr>
                <th colspan="3"><a href="?y=<?php echo h($year-1);?>">&laquo;</a>
                <?php echo h($year); ?>
                <a href="?y=<?php echo h($year+1);?>">&raquo;</a></th>
            </tr>
        </thead>
        <tbody>
            <tr>
<?php
for ($i = 1; $i <= 12; $i++) :
    $cal->create($i);
?>
                <td>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="7"><?php echo $i.'月'; ?></th>
                            </tr>
                            <tr>
                            <?php
                            foreach($cal->getWeekStr() as $str) {
                                echo "<th>".$str."</th>";
                            }
                            ?>
                            </tr>
                        </thead>
                        <tbody>
<?php
    foreach ($cal->getWeeks() as $week) {
        echo $week;
    }
?>
                        </tbody>
                    </table>
                </td>
<?php
    if ($i % 3 == 0) {
        echo "</tr><tr>";
    } elseif ($i == 12) {
        echo "</tr>";
    }
endfor;
?>
        </tbody> <!-- bodyここまで -->
    </table> <!-- frameここまで -->

<!-- JSポップ用 ID付与 style hide -->
<?php
foreach ($cal->getHolidayJs() as $holidayJs) {
    echo $holidayJs;
}
?>
<!-- style hide ここまで -->
</body>
</html>